using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;
using System;
using System.Diagnostics;

namespace task_1_driver
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            int timeout = 5;
            string videoName = "Massive volcanoes & Flamingo colony - Wild South America - BBC";
            IWebDriver chromeDriver = new ChromeDriver();
            chromeDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
            chromeDriver.Manage().Window.Maximize();
            chromeDriver.Navigate().GoToUrl("https://html.com/tags/iframe/");
            ((IJavaScriptExecutor)chromeDriver).ExecuteScript("window.scrollTo(0, 1000);");
            IWebElement video = chromeDriver.FindElement(By.XPath("//iframe[@class='lazy-loaded']"));
            chromeDriver.SwitchTo().Frame(video);
            video = chromeDriver.FindElement(By.XPath("//a[@class='ytp-title-link yt-uix-sessionlink']"));
            Assert.Equal(videoName, video.Text.Replace("&amp;", "&"));
            string VideoURL = video.GetAttribute("href");
            chromeDriver.Navigate().GoToUrl(VideoURL);
            string by = null;
            var watch = new Stopwatch();
            watch.Start();
            while (true)
            {
                try
                {
                    by = chromeDriver.FindElement(By.XPath("//span[@id='country-code']")).Text;
                    if(by != null || by != string.Empty)
                    {
                        break;
                    }
                }
                catch { }
                if(watch.Elapsed.TotalSeconds >= 5)
                {
                    watch.Stop();
                    break;
                }
            }
            Assert.Equal("BY", by);
            chromeDriver.Quit();
        }
    }
}
