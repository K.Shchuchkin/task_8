using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace task_2_driver
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            IWebDriver chromeDriver = new ChromeDriver();
            chromeDriver.Manage().Window.Maximize();
            chromeDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 1, 0);
            chromeDriver.Navigate().GoToUrl("http://github.com");
            chromeDriver.FindElement(By.XPath("//summary[@class='HeaderMenu-summary HeaderMenu-link px-0 py-3 border-0 no-wrap d-block d-lg-inline-block']")).Click();
            chromeDriver.SwitchTo().ActiveElement();
            chromeDriver.FindElement(By.XPath("//a[@data-ga-click='(Logged out) Header, go to Actions']")).Click();
            IWebElement element = chromeDriver.FindElement(By.XPath("//input[@class='form-control input-sm header-search-input jump-to-field js-jump-to-field js-site-search-focus ']"));
            element.Click();
            element.SendKeys("Selenium");
            element.Submit();
            string repoUri = chromeDriver.FindElement(By.XPath("//a[@class='v-align-middle']")).GetAttribute("href");
            ((IJavaScriptExecutor)chromeDriver).ExecuteScript("window.open();");
            chromeDriver.SwitchTo().Window(chromeDriver.WindowHandles.Last());
            chromeDriver.Navigate().GoToUrl(repoUri);
            chromeDriver.Navigate().Refresh();
            Assert.Equal("selenium", chromeDriver.FindElement(By.XPath("//a[@href='/SeleniumHQ/selenium']")).Text);
            ((IJavaScriptExecutor)chromeDriver).ExecuteScript("window.close();");
            chromeDriver.SwitchTo().Window(chromeDriver.WindowHandles.First());
            chromeDriver.Navigate().GoToUrl("https://selenium.dev/selenium/docs/api/dotnet/");
            using (var file = new StreamWriter("pageSource.html"))  //StreamWriter(new FileStream("pageSource.html", FileMode.OpenOrCreate, FileAccess.Write))
            {
                file.Write(chromeDriver.PageSource);
            }
            Assert.Contains("IHasInputDevices Interface", File.ReadAllText("pageSource.html"));
            chromeDriver.Quit();
        }
    }
}
